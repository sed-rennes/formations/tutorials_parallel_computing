# Multi-task programming with Pthread API

* You can find here a file [main.cpp](main.cpp) as start point for this tutorial

* Here we work with console standard output stream `std::cout` from STL (standard library in C++). If you don't know C++, don't worry: synthesis is simple to understand in our usage and be useful for illustrate some aspects of this tutorial

## Start exercise 

- Compiler and launch the executable with this  command :

 ```
 g++ main.cpp -lpthread -O3 -o tp_pthread
 ```

- What can be see on output console ?
## Multiples Threads
1.  Modify `main.cpp` to launch an arbitrary number of threads. This number must be defined by a constant, for example:

```C++
const long int numberThreads=3;
```

2.  Every secondary thread has to display, inside this output message, a ID number associated with itself : 

```c++
std::cout << "Hello World from thread "<< id <<" created by the main thread!" << std::endl;
```
- Use a parameter passed at thread creation to send ID number.
- Launch many runs with different threads numbers.
- What can be see on output console ?

3. Protect console output by Mutex
    - you need to add a **mutex** to the code as global.
    - lock the mutex before calls to `std::cout` and 
    - finally, release the mutex afer this
    - Check that outputs are well formed for different numbers of threads launched

## Ping & Pong using conditions and mutex (long exercise)


* In this exercise, you can modify a copy of precedent  exercise to launch to secondary threads `A` and `B`. These  two threads will simulate two tennis players that send each others the ball at every turn.
* **The bal position** is defined by a global variable `float X`.
* **The ball speed** is defined by a variable `float V` with a absolute value is equal to **2 m/s**

* The time step between two consecutive animations `float dT` is  **1 s**.
* For every animation step, a new value of `X` is computed by `X= V * dT + X`
* Every secondary thread has to switch de direction of the ball speed that move along one axis. 
    - Le Thread `A` switch the ball speed ( V = -V ) when ball reach the position `X = -10` 
m.
    - Le Thread `B` switch the ball speed ( V = -V ) when ball reach the position `X = 10` 
m.
* Every secondary thread has in charge the ball animation when the ball is in its camp:
    * If `X < 0` le thread A animates the ball
    * If `X >= 0` le thread B animates the ball
    * The thread who doesn't have the ball has to wait until ball is incoming its camp.
* The main thread is in charge of display at every time step `dT` the ball position.
* You need to work with **mutex** and associate **condition** to coordinate all threads. The aim of last is to keep minimum CPU load.
* To control animation speed, you can call the function `sleep(sec)` that allow to pause the thread for a `sec` number of seconds. To add this function in your code, you can include :
```c
#include <unistd.h>
```
