import pycuda.autoinit
import pycuda.driver as drv
import numpy

from pycuda.compiler import SourceModule

# Kernel definition and on-line compilation
mod = SourceModule("""
__global__ void reverse(float *destination, float *source, unsigned int size)
{
  const int global_i = blockIdx.x*blockDim.x +threadIdx.x;
  if ( size > global_i)
  {
    // TODO: find the way to reverse data order in destination array
    destination[global_i] = source[global_i];
  }
}
""")

reverse = mod.get_function("reverse")

size_data=100

# number of threads in a block
block_size=32
# number of blocks of thread
num_blocks=(size_data+block_size-1)//block_size

source = numpy.arange(0,size_data,1).astype(numpy.float32)
destination = numpy.zeros_like(source)
execution_time= reverse(
        drv.Out(destination), drv.In(source), numpy.uint32(size_data),
        block=(block_size,1,1), grid=(num_blocks,1), time_kernel=True)
# CPU "compute" same result in CPU and display the difference with GPU computing result
print(destination - source[::-1])
