## Goals of this tutorial  ##

1. Optimize C++ code using OpenMP and SIMD instructions set using python code modified in [SIMD tutorial](../SIMD/README.md)

# Get version of OpenMP :

Check the version of OpenMP with this command line : 
```bash
echo |cpp -fopenmp -dM |grep -i open
```
return will be something like : `#define _OPENMP 201511` 
Where the value is in format YYYYMM representing Year and Month of the specification release that you can found here: 

https://www.openmp.org//specifications/

## Add OpenMP pragma ##
1. Create a new implementation code of SAXPY named `openmp_code`
1. Try to to add OpenMP directives to allow parallel execution of loops in SAXPY code.
1. Determine what combination of parallel loop allow to get the better speed-up
1. Verify that result remains good in relation to reference code