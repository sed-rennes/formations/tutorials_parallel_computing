# tutorials_parallel_computing

These tutorials are designed to work on top of **Python** (except pthread tutorial). Python is high-level language very useful for scientific computation and rapid prototyping with **numpy** module. If you don't feel well with python language, don't worry, in these tutorials only the basics concepts of Python are needed and some explanations are given in important cases. If you want to know more about Python language please visit [Official Python web site](http://python.org/) or read [Google Python courses](https://developers.google.com/edu/python/)

Work with these tutorials supposes that you have a python interpreter installed with numpy and scipy modules as well as g++ compiler. For Cuda and OpenCL you have to install **PyCuda** and **PyOpenCL** modules. 

[Parallel Computing Courses Slides here](Introduction_to_parallel_computing.pdf)

## pthread simple tutorial
There is a simple tutorial for begin manipulate threads using POSIX Threads library [(pthread)](pthread/README.md)

## SIMD and OpenMP 
### Introduction to C++ SIMD instructions (Intel SSE or ARM Neo) using Python and inline compilation
There is a tutorial showing :
* how to include C++ code in python script with [inline module](https://github.com/GuillermoAndrade/inline)
* how to optimize C++ code using SIMD instructions and measure performance
[SIMD tutorial using Python and Inline module](SIMD/README.md)

### Multi-core programming with OpenMP and SIMD instructions using Python and Inline module 
There is a tutorial showing :
* how to optimize C++ code using OpenMP and SIMD instructions
[OpenMP and SIMD instructions tutorial using Python and Inline module](OpenMP/README.md)

## CUDA 
### Blocks and Grid on scalar multiplication kernel
The aim of this first tutorial is to learn how to define grids and blocks to work with a linear arbitrary size algebra problem (scalar multiplication) :

[CUDA Grid and Blocks tutorial](CUDA/blocks_and_grids.md)

### Reverse kernel and performance
The goal of this tutorial is to understand how pipeline continuity in memory access may improve performance is some algorithms using GPUs. For this tutorial will work with a naive reverse memory algorithm.

[Pipeline performance in reverse kernel](CUDA/Pipeline_performance_in_reverse_kernel.md)

## OpenCL

### Scan Amino Acid sequence
Naive scan of Animo Acid sequences for seeking fragment (protein) inside.

[Scan Amino Acid sequence](OpenCL/Scan_Amino_Acid_sequence.md)

### Brute force for solving a puzzle
*Work in progress*

Basic method for scan configurations of parts placement in a puzzle using OpenCL and comparison of architectures
[[Brute force for solving a puzzle]]
[[category: Documentation ]]


