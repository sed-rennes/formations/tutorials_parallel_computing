import numpy
import ctypes
from time import time
import inline
sizeX = 1000000
numberIterations =1000
X = numpy.random.rand(sizeX).astype(numpy.float32)
Y = numpy.empty(sizeX).astype(numpy.float32)



def get_assembly(code):
	return inline.cxx2asm(code, compiler_extra_args=['-march=native','-fopenmp', '-lstdc++']) 


def BenchmarkCode(name, code, X, Y, SIMD=True, OMP=True):
	# init
	Y[:]=0.5
	X[:]=1.0

	# compile the code
	compiler_extra_args = ['-g0','-lstdc++']
	link_extra_args = ['-lstdc++']
	if SIMD :
		compiler_extra_args+=['-march=native']
		link_extra_args += ['-march=native']
	if OMP:
		compiler_extra_args+=['-fopenmp']
		link_extra_args += ['-fopenmp']
	lib=inline.cxx(code, compiler_extra_args= compiler_extra_args, link_extra_args= link_extra_args) 
	p_float= numpy.ctypeslib.ndpointer(dtype=numpy.float32) 	
	lib.compute.argtypes = [ctypes.c_int, ctypes.c_int, p_float, p_float] 

	# print assembler code 
	asm_code=inline.cxx2asm(code, compiler_extra_args= compiler_extra_args) 
	with open(name+'.s','w') as f:
		f.write(asm_code)

	# start chronometer
	start_time = time()
	# run the code
	lib.compute(numberIterations, sizeX, X, Y)
	# stop chronometer             
	stop_time = time()
	execution_time= stop_time - start_time
	print("execution time for "+name+" code = "+ str(execution_time))
	return execution_time


# C++ reference code
referenceCode="""
#line 52 "saxpy.py" // helpful for debug
extern "C" {

#ifdef __SSE2__
#include<xmmintrin.h>
#endif
#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

void saxpy(int n, float alpha, float *X, float *Y)
{
	int i;
	for (i=0; i<n; i++)
		Y[i] += alpha * X[i];
}

void compute(int numberIterations, int sizeX, float *X, float *Y )
{
	for(int j=0; j< numberIterations;j++)
  		saxpy(sizeX, 0.001f, X, Y);
	return ;
}

}
"""

referenceTime=BenchmarkCode('Reference', referenceCode, X, Y)

SIMDCode="""
#line 82 "saxpy.py" // helpful for debug
extern "C" {

#ifdef __SSE2__
#include<xmmintrin.h>
#endif
#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

void saxpy(int n, float alpha, float *X, float *Y)
{
	int i;
	for (i=0; i<n; i++)
		Y[i] += alpha * X[i];
}


void compute(int numberIterations, int sizeX, float *X, float *Y )
{
	for(int j=0; j< numberIterations;j++)
  		saxpy(sizeX, 0.001f, X, Y);
	return ;
}

}
"""

SIMDTime=BenchmarkCode('SIMD', SIMDCode,X,Y)

print("speed up for SIMD = " + str(referenceTime/SIMDTime))

