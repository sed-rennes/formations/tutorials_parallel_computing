#!/usr/bin/env python
# -*- coding:utf-8 -*-

import atexit
import ctypes
import distutils.ccompiler
import os.path
import platform
import shutil
import sys
import tempfile


__version__ = '0.0.1'


def c(source, libraries=[], compiler_extra_args=[], link_extra_args=[]):
    r"""
    >>> c('int add(int a, int b) {return a + b;}').add(40, 2)
    42
    >>> sqrt = c('''
    ... #include <math.h>
    ... double _sqrt(double x) {return sqrt(x);}
    ... ''', ['m'])._sqrt
    >>> sqrt.restype = ctypes.c_double
    >>> sqrt(ctypes.c_double(400.0))
    20.0
    """
    path = _cc_build_shared_lib(source, '.c', libraries, 
        compiler_extra_args, link_extra_args)
    return ctypes.cdll.LoadLibrary(path)


def cxx(source, libraries=[], compiler_extra_args=[], link_extra_args=[]):
    r"""
    >>> cxx('extern "C" { int add(int a, int b) {return a + b;} }').add(40, 2)
    42
    """
    path = _cc_build_shared_lib(source, '.cc', libraries,
        compiler_extra_args, link_extra_args)
    return ctypes.cdll.LoadLibrary(path)

cpp = cxx  # alias

def cxx2asm(source, compiler_extra_args=[]):
    return _cc_get_assembly_code(source, '.cc', compiler_extra_args)

def c2asm(source, compiler_extra_args=[]):
    return _cc_get_assembly_code(source, '.c', compiler_extra_args)

def python(source):
    r"""
    >>> python('def add(a, b): return a + b').add(40, 2)
    42
    """
    obj = type('', (object,), {})()
    _exec(source, obj.__dict__, obj.__dict__)
    return obj


def _cc_get_assembly_code(source, suffix, compiler_extra_args):
    assembly_code=None
    tempdir = tempfile.mkdtemp()
    atexit.register(lambda: shutil.rmtree(tempdir))
    with tempfile.NamedTemporaryFile('w+', suffix=suffix, dir=tempdir) as f:
        f.write(source)
        f.seek(0)
        name=f.name
        cc = distutils.ccompiler.new_compiler()
        args = [] + compiler_extra_args
        if platform.system() == 'Linux':
            args.append('-fPIC')
            assembly_args = ['-S']
            assembly_file = cc.compile((name,), tempdir, extra_postargs=args+assembly_args)
            with open(assembly_file[0],'r') as fa:
                assembly_code=fa.read()
        return assembly_code

def _cc_build_shared_lib(source, suffix, libraries, 
    compiler_extra_args, link_extra_args, return_assembly_code=False):
    tempdir = tempfile.mkdtemp()
    atexit.register(lambda: shutil.rmtree(tempdir))
    with tempfile.NamedTemporaryFile('w+', suffix=suffix, dir=tempdir) as f:
        f.write(source)
        f.seek(0)
        name=f.name
        cc = distutils.ccompiler.new_compiler()
        args = [] + compiler_extra_args
        if platform.system() == 'Linux':
            args.append('-fPIC')
        obj = cc.compile((name,), tempdir, extra_postargs=args)
        for library in libraries:
            cc.add_library(library)
        cc.link_shared_lib(obj, name, tempdir, extra_postargs=link_extra_args)
        filename = cc.library_filename(name, 'shared')
        return os.path.join(tempdir, filename)

def _exec(object, globals, locals):
    r"""
    >>> d = {}
    >>> exec('a = 0', d, d)
    >>> d['a']
    0
    """
    if sys.version_info < (3,):
        exec('exec object in globals, locals')
    else:
        exec(object, globals, locals)
